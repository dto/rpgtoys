| temp |
temp := X11Color colorNames.
{
'SpringGreen4' -> #(0 139 69) .
'SteelBlue' -> #(70 130 180) .
'SteelBlue1' -> #(99 184 255) .
'SteelBlue2' -> #(92 172 238) .
'SteelBlue3' -> #(79 148 205) .
'SteelBlue4' -> #(54 100 139) .
'VioletRed' -> #(208 32 144) .
'VioletRed1' -> #(255 62 150) .
'VioletRed2' -> #(238 58 140) .
'VioletRed3' -> #(205 50 120) .
'VioletRed4' -> #(139 34 82) .
'WhiteSmoke' -> #(245 245 245).
'YellowGreen' -> #(154 205 50) .
'alice blue' -> #(240 248 255) .
'antique white' -> #(250 235 215) .
'aquamarine' -> #(127 255 212) .
'aquamarine1' -> #(127 255 212) .
'aquamarine2' -> #(118 238 198) .
'aquamarine3' -> #(102 205 170) .
'aquamarine4' -> #(69 139 116) .
'azure' -> #(240 255 255) .
'azure1' -> #(240 255 255) .
'azure2' -> #(224 238 238) .
'azure3' -> #(193 205 205) .
'azure4' -> #(131 139 139) .
'beige' -> #(245 245 220) .
'bisque' -> #(255 228 196) .
'bisque1' -> #(255 228 196) .
'bisque2' -> #(238 213 183) .
'bisque3' -> #(205 183 158) .
'bisque4' -> #(139 125 107) .
'black' -> #(0 0 0) .
'blanched almond' -> #(255 235 205) .
'blue violet' -> #(138 43 226) .
'blue' -> #(0 0 255) .
'blue1' -> #(0 0 255) .
'blue2' -> #(0 0 238) .
'blue3' -> #(0 0 205) .
'blue4' -> #(0 0 139) .
'brown' -> #(165 42 42) .
'brown1' -> #(255 64 64) .
'brown2' -> #(238 59 59) .
'brown3' -> #(205 51 51) .
'brown4' -> #(139 35 35) .
'burlywood' -> #(222 184 135) .
'burlywood1' -> #(255 211 155) .
'burlywood2' -> #(238 197 145) .
'burlywood3' -> #(205 170 125) .
'burlywood4' -> #(139 115 85) .
'cadet blue' -> #(95 158 160) .
'chartreuse' -> #(127 255 0) .
'chartreuse1' -> #(127 255 0) .
'chartreuse2' -> #(118 238 0) .
'chartreuse3' -> #(102 205 0) .
'chartreuse4' -> #(69 139 0) .
'chocolate' -> #(210 105 30) .
'chocolate1' -> #(255 127 36) .
'chocolate2' -> #(238 118 33) .
'chocolate3' -> #(205 102 29) .
'chocolate4' -> #(139 69 19) .
'coral' -> #(255 127 80) .
'coral1' -> #(255 114 86) .
'coral2' -> #(238 106 80) .
'coral3' -> #(205 91 69) .
'coral4' -> #(139 62 47) .
'cornflower blue' -> #(100 149 237) .
'cornsilk' -> #(255 248 220) .
'cornsilk1' -> #(255 248 220) .
'cornsilk2' -> #(238 232 205) .
'cornsilk3' -> #(205 200 177) .
'cornsilk4' -> #(139 136 120) .
'cyan' -> #(0 255 255) .
'cyan1' -> #(0 255 255) .
'cyan2' -> #(0 238 238) .
'cyan3' -> #(0 205 205) .
'cyan4' -> #(0 139 139) .
'dark blue' -> #(0 0 139) .
'dark cyan' -> #(0 139 139) .
'dark goldenrod' -> #(184 134 11) .
'dark gray' -> #(169 169 169) .
'dark green' -> #(0 100 0) .
'dark grey' -> #(169 169 169) .
'dark khaki' -> #(189 183 107) .
'dark magenta' -> #(139 0 139) .
'dark olive green' -> #(85 107 47) .
'dark orange' -> #(255 140 0) .
'dark orchid' -> #(153 50 204) .
'dark red' -> #(139 0 0) .
'dark salmon' -> #(233 150 122) .
'dark sea green' -> #(143 188 143) .
'dark slate blue' -> #(72 61 139) .
'dark slate gray' -> #(47 79 79) .
'dark slate grey' -> #(47 79 79) .
'dark turquoise' -> #(0 206 209) .
'dark violet' -> #(148 0 211) .
'deep pink' -> #(255 20 147) .
'deep sky blue' -> #(0 191 255) .
'dim gray' -> #(105 105 105) .
'dim grey' -> #(105 105 105) .
'dodger blue' -> #(30 144 255) .
'firebrick' -> #(178 34 34) .
'firebrick1' -> #(255 48 48) .
'firebrick2' -> #(238 44 44) .
'firebrick3' -> #(205 38 38) .
'firebrick4' -> #(139 26 26) 
 }
    do: [ :pair | temp add: pair ].
