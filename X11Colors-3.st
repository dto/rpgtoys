 "rgb.lisp --- X11 color data for XELF

 This file has been reformatted from its original version so as to
 be directly readable by Common Lisp, and is under the MIT
 License. The license and original copyright notice are reprinted
 below.

 Copyright (C) 1994 X Consortium

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the 'Software'), to deal in the Software without
 restriction, including without limitation the rights to use, copy,
 modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT.  IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR
 ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 Except as contained in this notice, the name of the X Consortium
 shall not be used in advertising or otherwise to promote the sale,
 use or other dealings in this Software without prior written
 authorization from the X Consortium.

 X Window System is a trademark of X Consortium, Inc."

| temp |
temp := X11Color colorNames.
{
'gray74' -> #(189 189 189) .
'gray75' -> #(191 191 191) .
'gray76' -> #(194 194 194) .
'gray77' -> #(196 196 196) .
'gray78' -> #(199 199 199) .
'gray79' -> #(201 201 201) .
'gray8' -> #(20 20 20) .
'gray80' -> #(204 204 204) .
'gray81' -> #(207 207 207) .
'gray82' -> #(209 209 209) .
'gray83' -> #(212 212 212) .
'gray84' -> #(214 214 214) .
'gray85' -> #(217 217 217) .
'gray86' -> #(219 219 219) .
'gray87' -> #(222 222 222) .
'gray88' -> #(224 224 224) .
'gray89' -> #(227 227 227) .
'gray9' -> #(23 23 23) .
'gray90' -> #(229 229 229) .
'gray91' -> #(232 232 232) .
'gray92' -> #(235 235 235) .
'gray93' -> #(237 237 237) .
'gray94' -> #(240 240 240) .
'gray95' -> #(242 242 242) .
'gray96' -> #(245 245 245) .
'gray97' -> #(247 247 247) .
'gray98' -> #(250 250 250) .
'gray99' -> #(252 252 252) .
'green yellow' -> #(173 255 47) .
'green' -> #(0 255 0) .
'green1' -> #(0 255 0) .
'green2' -> #(0 238 0) .
'green3' -> #(0 205 0) .
'green4' -> #(0 139 0) .
'grey' -> #(190 190 190) .
'grey0' -> #(0 0 0) .
'grey1' -> #(3 3 3) .
'grey10' -> #(26 26 26) .
'grey100' -> #(255 255 255) .
'grey11' -> #(28 28 28) .
'grey12' -> #(31 31 31) .
'grey13' -> #(33 33 33) .
'grey14' -> #(36 36 36) .
'grey15' -> #(38 38 38) .
'grey16' -> #(41 41 41) .
'grey17' -> #(43 43 43) .
'grey18' -> #(46 46 46) .
'grey19' -> #(48 48 48) .
'grey2' -> #(5 5 5) .
'grey20' -> #(51 51 51) .
'grey21' -> #(54 54 54) .
'grey22' -> #(56 56 56) .
'grey23' -> #(59 59 59) .
'grey24' -> #(61 61 61) .
'grey25' -> #(64 64 64) .
'grey26' -> #(66 66 66) .
'grey27' -> #(69 69 69) .
'grey28' -> #(71 71 71) .
'grey29' -> #(74 74 74) .
'grey3' -> #(8 8 8) .
'grey30' -> #(77 77 77) .
'grey31' -> #(79 79 79) .
'grey32' -> #(82 82 82) .
'grey33' -> #(84 84 84) .
'grey34' -> #(87 87 87) .
'grey35' -> #(89 89 89) .
'grey36' -> #(92 92 92) .
'grey37' -> #(94 94 94) .
'grey38' -> #(97 97 97) .
'grey39' -> #(99 99 99) .
'grey4' -> #(10 10 10) .
'grey40' -> #(102 102 102) .
'grey41' -> #(105 105 105) .
'grey42' -> #(107 107 107) .
'grey43' -> #(110 110 110) .
'grey44' -> #(112 112 112) .
'grey45' -> #(115 115 115) .
'grey46' -> #(117 117 117) .
'grey47' -> #(120 120 120) .
'grey48' -> #(122 122 122) .
'grey49' -> #(125 125 125) .
'grey5' -> #(13 13 13) .
'grey50' -> #(127 127 127) .
'grey51' -> #(130 130 130) .
'grey52' -> #(133 133 133) .
'grey53' -> #(135 135 135) .
'grey54' -> #(138 138 138) .
'grey55' -> #(140 140 140) .
'grey56' -> #(143 143 143) .
'grey57' -> #(145 145 145) .
'grey58' -> #(148 148 148) .
'grey59' -> #(150 150 150) .
'grey6' -> #(15 15 15) .
'grey60' -> #(153 153 153) .
'grey61' -> #(156 156 156) .
 }
    do: [ :pair | temp add: pair ].


    

