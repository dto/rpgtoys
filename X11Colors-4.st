 "rgb.lisp --- X11 color data for XELF

 This file has been reformatted from its original version so as to
 be directly readable by Common Lisp, and is under the MIT
 License. The license and original copyright notice are reprinted
 below.

 Copyright (C) 1994 X Consortium

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the 'Software'), to deal in the Software without
 restriction, including without limitation the rights to use, copy,
 modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT.  IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR
 ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 Except as contained in this notice, the name of the X Consortium
 shall not be used in advertising or otherwise to promote the sale,
 use or other dealings in this Software without prior written
 authorization from the X Consortium.

 X Window System is a trademark of X Consortium, Inc."

| temp |
temp := X11Color colorNames.
{
'orange3' -> #(205 133 0) .
'orange4' -> #(139 90 0) .
'orchid' -> #(218 112 214) .
'orchid1' -> #(255 131 250) .
'orchid2' -> #(238 122 233) .
'orchid3' -> #(205 105 201) .
'orchid4' -> #(139 71 137) .
'pale goldenrod' -> #(238 232 170) .
'pale green' -> #(152 251 152) .
'pale turquoise' -> #(175 238 238) .
'pale violet red' -> #(219 112 147) .
'papaya whip' -> #(255 239 213) .
'peach puff' -> #(255 218 185) .
'peru' -> #(205 133 63) .
'pink' -> #(255 192 203) .
'pink1' -> #(255 181 197) .
'pink2' -> #(238 169 184) .
'pink3' -> #(205 145 158) .
'pink4' -> #(139 99 108) .
'plum' -> #(221 160 221) .
'plum1' -> #(255 187 255) .
'plum2' -> #(238 174 238) .
'plum3' -> #(205 150 205) .
'plum4' -> #(139 102 139) .
'powder blue' -> #(176 224 230) .
'purple' -> #(160 32 240) .
'purple1' -> #(155 48 255) .
'purple2' -> #(145 44 238) .
'purple3' -> #(125 38 205) .
'purple4' -> #(85 26 139) .
'red' -> #(255 0 0) .
'red1' -> #(255 0 0) .
'red2' -> #(238 0 0) .
'red3' -> #(205 0 0) .
'red4' -> #(139 0 0) .
'rosy brown' -> #(188 143 143) .
'royal blue' -> #(65 105 225) .
'saddle brown' -> #(139 69 19) .
'salmon' -> #(250 128 114) .
'salmon1' -> #(255 140 105) .
'salmon2' -> #(238 130 98) .
'salmon3' -> #(205 112 84) .
'salmon4' -> #(139 76 57) .
'sandy brown' -> #(244 164 96) .
'sea green' -> #(46 139 87) .
'seashell' -> #(255 245 238) .
'seashell1' -> #(255 245 238) .
'seashell2' -> #(238 229 222) .
'seashell3' -> #(205 197 191) .
'seashell4' -> #(139 134 130) .
'sienna' -> #(160 82 45) .
'sienna1' -> #(255 130 71) .
'sienna2' -> #(238 121 66) .
'sienna3' -> #(205 104 57) .
'sienna4' -> #(139 71 38) .
'sky blue' -> #(135 206 235) .
'slate blue' -> #(106 90 205) .
'slate gray' -> #(112 128 144) .
'slate grey' -> #(112 128 144) .
'snow' -> #(255 250 250)  .
'snow1' -> #(255 250 250) .
'snow2' -> #(238 233 233) .
'snow3' -> #(205 201 201) .
'snow4' -> #(139 137 137) .
'spring green' -> #(0 255 127) .
'steel blue' -> #(70 130 180) .
'tan' -> #(210 180 140) .
'tan1' -> #(255 165 79) .
'tan2' -> #(238 154 73) .
'tan3' -> #(205 133 63) .
'tan4' -> #(139 90 43) .
'thistle' -> #(216 191 216) .
'thistle1' -> #(255 225 255) .
'thistle2' -> #(238 210 238) .
'thistle3' -> #(205 181 205) .
'thistle4' -> #(139 123 139) .
'tomato' -> #(255 99 71) .
'tomato1' -> #(255 99 71) .
'tomato2' -> #(238 92 66) .
'tomato3' -> #(205 79 57) .
'tomato4' -> #(139 54 38) .
'turquoise' -> #(64 224 208) .
'turquoise1' -> #(0 245 255) .
'turquoise2' -> #(0 229 238) .
'turquoise3' -> #(0 197 205) .
'turquoise4' -> #(0 134 139) .
'violet red' -> #(208 32 144) .
'violet' -> #(238 130 238) .
'wheat' -> #(245 222 179) .
'wheat1' -> #(255 231 186) .
'wheat2' -> #(238 216 174) .
'wheat3' -> #(205 186 150) .
'wheat4' -> #(139 126 102) .
'white smoke' -> #(245 245 245).
'white' -> #(255 255 255) .
'yellow green' -> #(154 205 50) .
'yellow' -> #(255 255 0) .
'yellow1' -> #(255 255 0) .
'yellow2' -> #(238 238 0) .
'yellow3' -> #(205 205 0) .
'yellow4' -> #(139 139 0)}
    do: [ :pair | temp add: pair ].


    

